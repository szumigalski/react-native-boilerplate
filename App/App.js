import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
import createStore from './Stores'
import RootScreen from './Containers/Root/RootScreen'
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import { mapping, light as lightTheme } from '@eva-design/eva'

const { store, persistor } = createStore()

export default class App extends Component {
  render() {
    return (
      <>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider mapping={mapping} theme={lightTheme}>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <RootScreen />
            </PersistGate>
          </Provider>
        </ApplicationProvider>
      </>
    )
  }
}
