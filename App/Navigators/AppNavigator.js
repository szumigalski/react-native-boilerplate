import { createAppContainer, createStackNavigator } from 'react-navigation'

import RootScreen from '../Containers/Root/RootScreen'

const StackNavigator = createStackNavigator(
  {
    MainScreen: RootScreen,
  },
  {
    initialRouteName: 'MainScreen',
    headerMode: 'none',
  }
)

export default createAppContainer(StackNavigator)
