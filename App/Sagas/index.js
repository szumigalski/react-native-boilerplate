import { takeLatest, all } from 'redux-saga/effects'
import { ExampleTypes } from 'App/Stores/Example/Actions'
import { fetchUser } from './ExampleSaga'

export default function* root() {
  yield all([takeLatest(ExampleTypes.FETCH_USER, fetchUser)])
}
