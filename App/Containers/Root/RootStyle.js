import styled from 'styled-components'
import { View } from 'react-native'

export const CenterView = styled(View)`
  justify-content: center;
  align-items: center;
  height: 100%;
`
