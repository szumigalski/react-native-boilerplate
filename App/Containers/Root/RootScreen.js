import React from 'react'
import { Text } from '@ui-kitten/components'
import { connect } from 'react-redux'
import { CenterView } from './RootStyle'

const RootScreen = () => {
  return (
    <CenterView>
      <Text>Main Page</Text>
    </CenterView>
  )
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(RootScreen)
